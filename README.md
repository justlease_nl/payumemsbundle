# PayumEmsBundle

This bundle compliments [justlease/payum-ems](https://bitbucket.org/justlease_nl/payumems) by making it easy to add to your Symfony project.

## Installation

```sh
$ composer require justlease/payum-ems-bundle
```

## Usage

Add PayumEmsBundle to your `AppKernel.php`:

```php
<?php

$bundles = [
    // ...
    new Payum\Bundle\PayumBundle\PayumBundle(),
    new Justlease\PayumEmsBundle\PayumEmsBundle(),
    // ...
];

```

Now you can configure your gateway through YAML:

```yaml
payum:
    gateways:
        ems_offsite:
            factory: ems_offsite
            store_name: '123456789'
            shared_secret: 'mysharedsecret'
            sandbox: true

```

For more configuration options, please refer to [justlease/payum-ems](https://bitbucket.org/justlease_nl/payumems).
