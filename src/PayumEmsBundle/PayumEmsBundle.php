<?php

declare(strict_types=1);

namespace Justlease\PayumEmsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

final class PayumEmsBundle extends Bundle
{
}
