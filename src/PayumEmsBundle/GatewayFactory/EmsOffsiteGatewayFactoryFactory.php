<?php

declare(strict_types=1);

namespace Justlease\PayumEmsBundle\GatewayFactory;

use Justlease\PayumEms\EmsOffsiteGatewayFactory;
use Payum\Core\GatewayFactoryInterface;

final class EmsOffsiteGatewayFactoryFactory
{
    public static function create(): callable
    {
        return function (array $defaultConfig, GatewayFactoryInterface $gatewayFactory) {
            return new EmsOffsiteGatewayFactory($defaultConfig, $gatewayFactory);
        };
    }
}
